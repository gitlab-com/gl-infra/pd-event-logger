FROM golang:1.22 as builder

WORKDIR /app

# Retrieve application dependencies.
# Copy local code to the container image.
COPY . ./
# Build the binary.
RUN cd cmd && CGO_ENABLED=0 GOOS=linux go build -mod=readonly -v -o pd-event-logger

FROM alpine:3
RUN apk add --no-cache ca-certificates
COPY --from=builder /app/cmd/pd-event-logger /pd-event-logger

ENV PORT 3000
# Run the web service on container startup.
CMD ["/pd-event-logger"]
