package esevent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	pagerduty "github.com/PagerDuty/go-pagerduty"
	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-com/gl-infra/pagerduty-event-logger/internal/logger"
)

var log *logrus.Logger

type EventSender interface {
	DecodeAndSend(string) error
}

type event struct {
	Client *http.Client
	url    string
	env    string
}

// PDEvent contains a representation of a PagerDuty webhook that is logged to the event index
type PDEvent struct {
	Time      string `json:"time"`
	Type      string `json:"type"`
	Message   string `json:"message"`
	Env       string `json:"env"`
	Status    string `json:"status"`
	Source    string `json:"source"`
	Username  string `json:"username"`
	Assignees string `json:"assignees"`
	Service   string `json:"service"`

	AlertLabels map[string]interface{} `json:"alert_labels"`
}

func New(esURL, esIndex, env string) EventSender {
	log = logger.New()
	return &event{
		Client: &http.Client{},
		url:    fmt.Sprintf("%s/%s/_doc", esURL, esIndex),
		env:    env,
	}
}

func (e *event) DecodeAndSend(payload string) error {
	jsonData := strings.NewReader(payload)
	res, err := pagerduty.DecodeWebhook(jsonData)

	if err != nil {
		return err
	}

	for _, payload := range e.payloadsFromRes(res) {
		log.Info("Sending request")
		log.Debug(payload)

		var jsonStr = []byte(payload)
		req, err := http.NewRequest("POST", e.url, bytes.NewBuffer(jsonStr))
		if err != nil {
			log.Panic(err)
		}

		req.Header.Set("Content-Type", "application/json")
		resp, err := e.Client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
	}
	return nil
}

func (e *event) payloadsFromRes(res *pagerduty.WebhookPayloadMessages) []string {
	payloads := []string{}

	for _, msg := range res.Messages {
		details := make(map[string]interface{})
		for _, logEntry := range msg.LogEntries {
			if logEntry.Type != "trigger_log_entry" || logEntry.Channel.Type != "api" {
				continue
			}
			channelRaw := logEntry.Channel.Raw
			if detailsRaw, ok := channelRaw["details"]; ok {
				if detailsStr, ok := detailsRaw.(map[string]interface{}); ok {
					details = detailsStr
					break
				}
			}
		}
		delete(details, "firing")
		delete(details, "note")

		inc := msg.Incident

		assignees := []string{}
		for _, assignee := range inc.Assignments {
			assignees = append(assignees, assignee.Assignee.Summary)
		}

		pdEvent := PDEvent{
			Time:      inc.LastStatusChangeAt.Format("2006-01-02T15:04:05.000Z"),
			Type:      "pagerduty",
			Message:   inc.Title,
			Env:       e.env,
			Status:    inc.Status,
			Source:    inc.HTMLURL,
			Username:  inc.LastStatusChangeBy.Summary,
			Assignees: strings.Join(assignees[:], ", "),
			Service:   inc.Service.Summary,

			AlertLabels: details,
		}

		payload, _ := json.Marshal(pdEvent)
		payloads = append(payloads, string(payload[:]))
	}

	return payloads
}
