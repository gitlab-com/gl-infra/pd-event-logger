package esevent

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"testing"
)

func TestSend(t *testing.T) {
	tests := []struct {
		name    string
		payload string
		want    *PDEvent
	}{
		{
			name:    "curl with labels",
			payload: "testdata/payload-curl-with-labels.json",
			want: &PDEvent{
				Time:      "2021-09-22T13:11:50.000Z",
				Type:      "pagerduty",
				Message:   "test alert",
				Env:       "some-env",
				Status:    "triggered",
				Source:    "https://gitlab.pagerduty.com/incidents/PWVFT54",
				Username:  "iwiedler-test",
				Assignees: "Igor Wiedler",
				Service:   "iwiedler-test",

				AlertLabels: map[string]interface{}{"foo": "bar"},
			},
		},
		{
			name:    "curl without labels",
			payload: "testdata/payload-curl-without-labels.json",
			want: &PDEvent{
				Time:      "2021-09-23T10:10:28.000Z",
				Type:      "pagerduty",
				Message:   "test alert",
				Env:       "some-env",
				Status:    "triggered",
				Source:    "https://gitlab.pagerduty.com/incidents/PWQEHIH",
				Username:  "iwiedler-test",
				Assignees: "Igor Wiedler",
				Service:   "iwiedler-test",

				AlertLabels: map[string]interface{}{},
			},
		},
		{
			name:    "web",
			payload: "testdata/payload-web.json",
			want: &PDEvent{
				Time:      "2021-09-23T10:16:33.000Z",
				Type:      "pagerduty",
				Message:   "test incident, triggered via web",
				Env:       "some-env",
				Status:    "triggered",
				Source:    "https://gitlab.pagerduty.com/incidents/PQTYIBT",
				Username:  "iwiedler-test",
				Assignees: "Igor Wiedler",
				Service:   "iwiedler-test",

				AlertLabels: map[string]interface{}{},
			},
		},
		{
			name:    "alertmanager",
			payload: "testdata/payload-alertmanager.json",
			want: &PDEvent{
				Time:      "2021-09-23T14:49:46.000Z",
				Type:      "pagerduty",
				Message:   "Firing 1 - The HPA Desired Replicas resource of the sidekiq service (main stage), component has a saturation exceeding SLO and is close to its capacity limit.",
				Env:       "some-env",
				Status:    "triggered",
				Source:    "https://gitlab.pagerduty.com/incidents/P7KECXO",
				Username:  "GitLab Production",
				Assignees: "Rehab",
				Service:   "GitLab Production",

				AlertLabels: map[string]interface{}{
					"alertname":           "component_saturation_slo_out_of_bounds",
					"component":           "kube_hpa_desired_replicas",
					"feature_category":    "",
					"num_firing":          "1",
					"num_resolved":        "0",
					"product_stage":       "",
					"product_stage_group": "",
					"resolved":            "",
					"stage":               "main",
					"tier":                "sv",
					"type":                "sidekiq",
					"user_impacting":      "",
				},
			},
		},
	}

	var pdEvent *PDEvent

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		got := req.URL.Path
		want := "/some-index/_doc"
		if got != want {
			t.Errorf("path %q is not %q", got, want)
		}

		buf := new(bytes.Buffer)
		_, err := buf.ReadFrom(req.Body)
		if err != nil {
			t.Errorf("Unable to read request body: %v", err)
		}

		pdEvent = &PDEvent{}
		err = json.Unmarshal(buf.Bytes(), pdEvent)
		if err != nil {
			t.Errorf("Unable to decode ElasticSearch event data: %v", err)
		}
	}))
	defer server.Close()

	event := New(server.URL, "some-index", "some-env")
	log.SetOutput(io.Discard)

	for _, tt := range tests {
		pdEvent = nil
		t.Run(tt.name, func(t *testing.T) {
			jsonStr, err := os.ReadFile(tt.payload)
			if err != nil {
				t.Errorf("Unable to read payload: %v", err)
			}

			err = event.DecodeAndSend(string(jsonStr[:]))
			if err != nil {
				t.Errorf("Failed to send event: %v", err)
			}

			if !reflect.DeepEqual(pdEvent, tt.want) {
				t.Errorf("Events do not match, %q is not %q", pdEvent, tt.want)
			}
		})
	}
}
