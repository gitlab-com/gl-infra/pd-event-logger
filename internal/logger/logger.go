package logger

import (
	"github.com/sirupsen/logrus"
)

type UTCFormatter struct {
	defaultFormatter logrus.Formatter
}

func (f *UTCFormatter) Format(entry *logrus.Entry) ([]byte, error) {
	entry.Time = entry.Time.UTC()
	return f.defaultFormatter.Format(entry)
}

func New() *logrus.Logger {
	l := logrus.New()
	l.SetLevel(logrus.DebugLevel)
	l.SetFormatter(&UTCFormatter{
		defaultFormatter: &logrus.TextFormatter{
			TimestampFormat: "2006-01-02T15:04:05.000Z",
			FullTimestamp:   true,
		},
	})
	return l
}
