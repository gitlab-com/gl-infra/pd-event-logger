package server

import (
	"bytes"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

type StubEventSender struct {
	events []string
}

func (s *StubEventSender) DecodeAndSend(event string) error {
	s.events = append(s.events, event)
	return nil
}

func TestWebhook(t *testing.T) {
	jsonPostData := []byte(`{"some-key": "some-value"}`)
	req, _ := http.NewRequest(http.MethodPost, "/", bytes.NewBuffer(jsonPostData))
	response := httptest.NewRecorder()

	req.Header.Set("X-Event-token", "some-token")

	stubEventSender := &StubEventSender{}

	s := New("some-token", stubEventSender)
	log.SetOutput(io.Discard)

	s.ServeHTTP(response, req)

	if response.Code != 200 {
		t.Fatalf("got %d, expected 200 for status code", response.Code)
	}

	if len(stubEventSender.events) != 1 {
		t.Fatalf("got %d events, expected 1", len(stubEventSender.events))
	}
}
