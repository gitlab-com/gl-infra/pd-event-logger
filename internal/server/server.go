package server

import (
	"io"
	"net/http"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-com/gl-infra/pagerduty-event-logger/internal/logger"
)

var log *logrus.Logger

type EventServer struct {
	esEvent    EventSender
	eventToken string
}

type EventSender interface {
	DecodeAndSend(string) error
}

func New(eventToken string, eventSender EventSender) *EventServer {
	log = logger.New()
	eventServer := EventServer{
		eventToken: eventToken,
		esEvent:    eventSender,
	}

	return &eventServer
}

func (e *EventServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Invalid request method.", 405)
		return
	}

	token := r.Header.Get("X-Event-token")

	if len(e.eventToken) == 0 {
		log.Warn("EVENT_TOKEN not set in environment, disabling authentication")
	} else {
		if token != e.eventToken {
			http.Error(w, "Incorrect token for request", 403)
			return
		}
	}

	buf := new(strings.Builder)
	_, _ = io.Copy(buf, r.Body)

	err := e.esEvent.DecodeAndSend(buf.String())

	if err != nil {
		http.Error(w, "Unable to decode and send data", 500)
		log.Error(err)
	}
}
