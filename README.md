# pagerduty-event-logger

Sets up a webhook for PagerDuty to forward alerts to ElasticSearch

CI deploys this to GCP CloudRun in the `gitlab-infra-automation-stg` and `gitlab-infra-automation` account

## CI Variables Required

- `ES_NONPROD_EVENTS_URL`: URL with authentication for the nonprod ElasticSearch cluster (see 1pass)
- `EVENT_TOKEN`: Token used to authenticate webhook requests, is set in the `X-Event-token` header (see 1pass)
- `CLOUDSDK_AUTH_CREDENTIAL_FILE_OVERRIDE`: service account json key for the `pd-event-logger` service account with `Cloud Run Admin`, `Cloud Run Service Agent` and `Storage Admin` permissions
