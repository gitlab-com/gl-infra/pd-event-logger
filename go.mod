module gitlab.com/gitlab-com/gl-infra/pagerduty-event-logger

go 1.22

require (
	github.com/PagerDuty/go-pagerduty v1.3.0
	github.com/sirupsen/logrus v1.4.2
)

require (
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	golang.org/x/sys v0.0.0-20190422165155-953cdadca894 // indirect
)
