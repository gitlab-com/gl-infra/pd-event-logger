package main

import (
	"fmt"
	"net/http"
	"os"
	"regexp"

	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-com/gl-infra/pagerduty-event-logger/internal/esevent"
	"gitlab.com/gitlab-com/gl-infra/pagerduty-event-logger/internal/logger"
	"gitlab.com/gitlab-com/gl-infra/pagerduty-event-logger/internal/server"
)

const (
	defaultIndex = "events-gstg"
	defaultPort  = "3000"
)

var log *logrus.Logger

func main() {
	log = logger.New()
	listenPort := os.Getenv("PORT")
	if len(listenPort) == 0 {
		listenPort = defaultPort
	}

	esURL := os.Getenv("ES_NONPROD_EVENTS_URL")
	if len(esURL) == 0 || len(listenPort) == 0 {
		log.Panic("You must set ES_NONPROD_EVENTS_URL")
	}

	eventToken := os.Getenv("EVENT_TOKEN")

	esIndex := os.Getenv("ES_INDEX")
	if len(esIndex) == 0 {
		log.Warnf("ES_INDEX not set, setting event index to %s", defaultIndex)
		esIndex = defaultIndex
	}

	// Extract the environment name from the index
	// There are only two valid indexes so we check for that as well
	re := regexp.MustCompile("events-(gstg|gprd)")
	match := re.FindStringSubmatch(esIndex)
	if len(match) == 0 {
		log.Panicf("Unable to determine appropriate ES index from `%s`", esIndex)
	}

	log.Info("Started listener on port 3000")
	eventSender := esevent.New(esURL, esIndex, match[1])
	server := server.New(eventToken, eventSender)
	_ = http.ListenAndServe(fmt.Sprintf(":%s", listenPort), server)
}
